package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling http request.
@RestController
//Will require all routes within the class to use the set endpoint as part of its route.
@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

//	ctrl + c to stop the server and application

//	localhost:8080/hello
	@GetMapping("/hello")
//	Maps a get request to the route "/hello" and invokes the method hello().
	public String hello(){
		return "Hello World";
	}

//	Route with String Query
//	localhost:8080/hi?name=value
	@GetMapping("/hi")
//	"@RequestParam" annotation that allows us to extract data from query strings in the URL
	public String hi(@RequestParam(value="name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

//	Multiple parameters
//	localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue = "Joe") String name, @RequestParam(value="friend", defaultValue = "Jane") String friend){
		return  String.format("Hello %s! My name is %s.", friend, name);
	}

//	Route with path variables
//	Dynamic data is obtained directly from the url
//	localhost:8080/name
	@GetMapping("/hello/{name}")
//	"@PathVariable" annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s!", name);
	}

//	Activity for s09
@GetMapping("/welcome/")
public String welcome(@RequestParam("user") String user, @RequestParam("role") String role) {
	String roles = role;
	switch (roles){
		case "admin": return String.format("Welcome back to the class portal, Admin %s ", user);
		case "teacher": return String.format("Thank you for logging in, Teacher %s ", user);
		case "student": return String.format("Welcome back to the class portal, %s ", user);
		default: return String.format("Role out of range");
	}
}

	private ArrayList<Student> students = new ArrayList<>();
	@GetMapping("/register")
	public String register(@RequestParam("id") String id, @RequestParam("name") String name,@RequestParam("course") String course) {
		Student student = new Student(id, name, course);
		students.add(student);
		return String.format("%s your id number is registered on the system",id);
	}
	public String account(@PathVariable("id") String id) {
		for (Student student : students) {
			if (student.getId().equals(id)) {
				return String.format("Welcome back %s! You are currently enrolled in s%",student.getName(),student.getCourse());
			}
		}
		return String.format("Your provided  %s is not found in the system!",id);
	}




}
